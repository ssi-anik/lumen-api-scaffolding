# Lumen Scaffolding

## Lumen Version
This repository contains Lumen `6.x`

## Dependencies, requirements and build tools
This project comes with `docker` & `docker-compose`. But to minimize the boot up time when you try `docker-compose up -d --build` the local files are mounted to application & worker containers and **NOT COPIED TO CONTAINERS**.
Thus, it's recommended to use PHP & composer locally. Resolve your project dependency before you run your application using `composer install`.

This project contains,
- `nginx` for web server.
- `postgres` for database.
- `redis` for cache.
- `beanstalkd` for queue driver.
- `beanstalk-console` as beanstalk's admin tool.

## How to use?
- Clone the repository.
- `cp docker-compose.yml.example docker-compose.yml`.
- Make the required changes to your `docker-compose.yml`.
- `cp .env.example .env`.
- Make the required changes to your `.env`.
- `docker-compose up -d --build` to build your containers.
- Run `php artisan key:generate` to generate application key.
- Loading `http://127.0.0.1:{NGINX_PORT}` in your browser will return a json response.
- If you don't have `composer` locally, then `exec`-ing to php container after containers are up and install the dependencies will work. Just restart the containers.

## Understanding Environments
- `REPORT_TO_SENTRY` - set to `false` if you don't want to use sentry.
- `SENTRY_LARAVEL_DSN` - to send your error messages to sentry.
- `JWT_*` - settings for JWT package it uses.
- `REDIS_*` - configuration for `redis` as cache.
- `BS_*` - configuration for Beanstalk for your queue driver.
- `BS_DEFAULT_QUEUE` - if you run your queue supervisor without providing queue name, set comma separated values here.
- `QUEUE_REDIS_CONNECTION` - Redis connection name if you want to use `redis` as your queue driver.
- `QUEUE_REDIS_*` - Will use these settings for redis queue driver set by `QUEUE_REDIS_CONNECTION`
- `QUEUE_REDIS_DEFAULT_QUEUE` - if you run your queue supervisor without any queue name, set comma separated values here.
- `ENABLE_QUERY_LOG` - set `false` to disable or `true` to enable. `daily` to **enable** and log in ./storage/logs/query-{Y-m-d}.log file.
- `LOG_HTTP_REQUEST` - set `true` to enable or `false` to disable when using `App\Service\AbstractApiService` on behalf of APIZ package to Communicate with remote service.
- `SERVICE_*` - Timeouts when using `App\Service\AbstractApiService` class on behalf of APIZ.
- `VERIFY_SSL` - SSL Verification when using `App\Service\AbstractApiService` class on behalf of APIZ.

## What if I don't want to use a few services?

### NewRelic
- In `bootstrap/app.php`, change

```php
$app->instance(Illuminate\Contracts\Debug\ExceptionHandler::class,
    new Nord\Lumen\ChainedExceptionHandler\ChainedExceptionHandler(new App\Exceptions\Handler(), [
        new Nord\Lumen\NewRelic\NewRelicExceptionHandler(),
    ]));
```
to
```php
$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    App\Exceptions\Handler::class
);
```

- In `bootstrap/app.php`, remove `Nord\Lumen\NewRelic\NewRelicMiddleware::class` from `$app->middleware()` array.
- In `bootstrap/app.php`, comment `$app->register(Nord\Lumen\NewRelic\NewRelicServiceProvider::class);`.
- Remove `nordsoftware/lumen-newrelic` from `composer.json`.

### Sentry
- In `bootstrap/app.php`, remove the following block.

```php
if (report_to_sentry()) {
    $app->register(Sentry\Laravel\ServiceProvider::class);
}
```

- In `app\Exceptions\Handler.php` remove the following.
```php
if (report_to_sentry() && app()->bound('sentry') && $this->shouldReport($exception)) {
    app('sentry')->captureException($exception);
}
```
- Remove `sentry/sentry-laravel` from your `composer.json`.

### CORS
- In `bootstrap/app.php`, remove `$app->configure('cors');`
- In `bootstrap/app.php`, remove `Nord\Lumen\Cors\CorsMiddleware::class` from `$app->middleware()` array.
- Remove `nordsoftware/lumen-cors` from your `composer.json`.

### Query logs
- In `bootstrap/app.php`, remove the following block.
```php
if (log_db_queries()) {
    $app->register(App\Providers\QueryLoggerServiceProvider::class);
    $app->middleware(App\Http\Middleware\QueryLoggerMiddleware::class);
}
```

## Keep in mind
- Use separate docker file for your **PRODUCTION**, in which you'll **copy your code** and not **mount** volumes.
- Enable `opcache` for better performance and remove `nano`, `unzip`, `zip`, `git`, `composer` in your production dockerfile.
- Change `docker/php/conf.ini` according to your need.
- The `app` service in `docker-compose` file, `app.build.args` takes NewRelic information. You can use it to install and integrate newrelic in your application. Only installs if the `app.build.args.ENVIRONMENT` is `production`.
- Don't use `Facade`s, use `app('facade-accessor')` instead.

## Coding styles
- APIs are defined in `routes/api.php` file.
- The **Remote Http Services** goes under `app/Services` directory.
- Helper methods are in `app/Extensions/methods.php` file. Method names are snake_cased.
- Helper classes are in `app/Extensions` directory. Create single class in each file.
- Use cache whenever needed and possible. Use `App\Services\CacheService` class. Define the cache key at the top of that class and use that key within that class whenever required. This way you don't need to change the key everywhere. Change in one place. You can use `keyResolver` method in that class which will replace the placeholder.
- Models are defined under the `app/Models` directory.
- Use **Events** whenever possible.
- Use **Jobs**. Time consuming or not necessary for response tasks should be pushed to the jobs.
- The `auth` will be resolved automatically and will give you the current logged in user if token is provided. `auth()->user()`.
- Use composer's **require** and **require-dev** based on requirement.
- If some provider or tasks don't make sense in local, use `in_environment` or `is_production` or `is_local` methods to run in the right environment.
- Try to develop as much dynamic as possible. Trigger features reading from environment if possible.
- All the environment variables are collected from `config('settings')` configuration. No environment is consumed directly even tho it doesn't matter in Lumen.
- Make sure the added environment stays in `.env.example` file too.
- Missing **Laravel commands** are included. You can use them if required.
- Please use proper formatting & indentation. Use early return when possible.
- Use **space** instead of **tabs**.
- For strings, use **single quotes** rather than **double quotes**. It doesn't process string parsing which leads to run code faster.
- Use `.gitignore` wisely.
- Use meaningful **variables** & **class names**.
- Use **FormRequest** whenever required. Keep the Controllers clean.
- If a variable is never used, don't use that variable. [It takes memory](https://medium.com/@sirajul.anik/how-does-laravel-jobs-get-queued-c241b04135b7).
- Try DRY.
