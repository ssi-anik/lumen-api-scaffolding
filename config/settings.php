<?php

return [
    'enable_query_log' => env('ENABLE_QUERY_LOG', false),
    '2fa'              => [
        'enabled' => (bool) env('ENABLE_2FA', false),
        'mediums' => env('TWO_FA_MEDIUMS', ''),
        'otp_ttl' => (int) env('TTL_2FA', 3) * 60,
    ],

    'service' => [
        'response_timeout'   => env('SERVICE_RESPONSE_TIMEOUT', 5),
        'connection_timeout' => env('SERVICE_CONNECTION_TIMEOUT', 5),
        'verify_ssl'         => env('VERIFY_SSL', false),
        'log_request'        => env('LOG_HTTP_REQUEST', false),
    ],

    's3' => [
        'enabled' => (bool) env('S3_ENABLED'),
        'expiry'  => sprintf('+%s', ltrim(env('S3_SIGN_EXPIRY', '+10 minutes'), '+')), // trim the + sign by default.
    ],

    'ttl' => [
        'auth_user' => (int) env('AUTH_USER_TTL', 60) * 60,
    ],
];