<?php

return [

    'defaults' => [
        'guard' => env('AUTH_GUARD', 'api'),
    ],

    'guards' => [
        'api' => [
            'driver'   => 'jwt',
            'provider' => 'cached-user',
        ],
    ],

    'providers' => [
        'users'       => [
            'driver' => 'eloquent',
            'model'  => App\Models\User::class,
        ],
        'cached-user' => [
            'driver' => 'cached-user-driver',
            'model'  => App\Models\User::class,
        ],
    ],

    'passwords' => [],
];
