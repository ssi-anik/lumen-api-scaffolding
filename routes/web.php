<?php

/** @var \Laravel\Lumen\Routing\Router $router */
$router->get('/', function () use ($router) {
    return [
        'app'         => config('app.name'),
        'framework'   => $router->app->version(),
        'api-version' => 'v1',
    ];
});
