<?php

namespace App\Providers;

use App\Services\CacheService;
use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Support\ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    public function register () {
        $this->app['auth']->provider('cached-user-driver', function ($app, array $config) {
            $provider = new EloquentUserProvider($this->app['hash'], $config['model']);
            $cache = app(CacheService::class);

            return new CachedAuthUserProvider($provider, $cache);
        });
    }

    public function boot () {
        //
    }
}
