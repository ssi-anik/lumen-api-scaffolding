<?php

namespace App\Services;

use App\Exceptions\MissingValueException;
use App\Extensions\OtpStorage;
use App\Models\Concept;
use App\Models\Member;
use App\Models\User;
use Illuminate\Contracts\Cache\Repository;

class CacheService
{
    private static $KEY_AUTH_USER = 'auth-user-%d'; // auth-user-{id}

    private $repository;

    public function __construct (Repository $repository) {
        $this->repository = $repository;
    }

    private function keyResolver (string $specifier, ...$values) : string {
        return sprintf($specifier, ...$values);
    }

    public function getDataByKey ($key) {
        return $this->repository->get($key);
    }

    public function incrementByKey ($key, $ttl) {
        if (!$this->getDataByKey($key)) {
            $this->repository->set($key, 0, $ttl);
        }

        return $this->repository->increment($key);
    }

    public function setDataByKey ($key, $data, $ttl) {
        $this->repository->set($key, $data, $ttl);
    }

    public function forgetDataByKey ($key) {
        $this->repository->forget($key);
    }

    public function setAuthUser (int $id, User $user, $ttl = null) : void {
        $key = $this->keyResolver(self::$KEY_AUTH_USER, $id);

        $this->repository->set($key, $user, $ttl);
    }

    public function getAuthUser (int $id) : ?User {
        $key = $this->keyResolver(self::$KEY_AUTH_USER, $id);

        return $this->repository->get($key);
    }

    public function forgetAuthUser (int $id) : bool {
        $key = $this->keyResolver(self::$KEY_AUTH_USER, $id);

        return $this->repository->delete($key);
    }
}